#include <windows.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    const char *desktop_name = "Win32Headless";
    HDESK desk = CreateDesktopA(desktop_name, NULL, NULL, 0, MAXIMUM_ALLOWED, NULL);

    STARTUPINFOA si = {};
    si.cb = sizeof(si);
    si.lpDesktop = (char *)desktop_name;

    PROCESS_INFORMATION pi;
    if (!CreateProcessA(NULL, argv[1], NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
        fprintf(stderr, "failed to start '%s'\n", argv[1]);
        exit(1);
    }

    if (WaitForSingleObject(pi.hProcess, INFINITE) != WAIT_OBJECT_0) {
        fprintf(stderr, "failed to wait for '%s'\n", argv[1]);
        exit(1);
    }

    DWORD exit_code;
    if (!GetExitCodeProcess(pi.hProcess, &exit_code)) {
        fprintf(stderr, "failed to get exit-code for '%s'\n", argv[1]);
        exit(1);
    }

    fprintf(stderr, "exit code for '%s': %d\n", argv[1], exit_code);
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
    exit((int)exit_code);
}
